extern crate chrono;
extern crate rocket;
extern crate rocket_contrib;
#[macro_use] extern crate serde;
extern crate serde_json;

pub mod io;
pub mod properties;

use rocket::{ form::FromForm };
use serde::{ Deserialize, Serialize };
use std::hash::{ Hash };

#[derive(Clone, Debug, Deserialize, Hash)]
pub struct Meta { pub app_name: String, pub api_key: String }
