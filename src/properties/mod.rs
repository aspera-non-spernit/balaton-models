use chrono::{ NaiveDate };
// Property example
// [
//     {
//         "id" : 0,
//         "name" : "David Bowie Estate 1",
//         "address" : {
//             "street" : "David-Bowie-Promenade",
//             "postcode" : "10289",
//             "city" : "Berlin",
//             "state" : "Berlin",
//             "country" : "Germany"
//         },
//         "minimum_rent_term" : "OneDay",
//         "tenant_history" : [
//             {
//                 "first_name" : "David",
//                 "last_name" : "Bowie",
//                 "move_in" : "1977-01-01",
//                 "move_out" : "1979-12-31",
//                 "security_deposit": 200.0,
//                 "term_rent": 77.0
//             }
//         ]
//     }
// ]

#[derive(Debug, Deserialize, Serialize)]
pub struct Property {
    pub id: usize,
    pub name: String,
    pub address: Address,
    pub tenant_history: TenantHistory,
    pub minimum_rent_term: MinimumRentTerm
}

#[derive(Debug, Deserialize, Serialize)]
pub struct Address {
    pub street: String,
    pub postcode: String,
    pub city: String,
    pub state: String,
    pub country: String,
}

#[derive(Debug, Deserialize, Serialize)]
pub struct TenantHistory {
    pub first_name: String,
    pub last_name: String,
    pub move_in: NaiveDate,
    pub move_out: NaiveDate,
    pub security_deposit: f32,
    pub term_rent: f32
}

#[derive(Debug, Deserialize, Serialize)]
pub enum MinimumRentTerm {
    OneDay, OneWeek, OneMonth, OneYear
}