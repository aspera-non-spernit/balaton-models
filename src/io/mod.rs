#[derive(Deserialize, Serialize)]
pub struct BalatonRequest<'s> {
    pub path: &'s str,
    pub query:  &'s str
}