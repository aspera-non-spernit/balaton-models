use serde::{ Deserialize, Serialize };
use std::fmt::Debug;

#[derive(Clone, Debug, Deserialize, Hash)]
pub enum Role { Admin }

#[derive(Debug, Deserialize)]
pub struct Users { pub data: Vec<User> }

#[derive(Clone, Debug, Deserialize)]
pub struct User {
    pub credentials: Credentials,
    pub hash: String,
    pub role: Role,
    pub auth: Option<String>
}

impl Hash for &User {
    fn hash<H: Hasher>(&self, state: &mut H) {
        self.credentials.hash(state);
        self.role.hash(state);
        self.auth.hash(state);
    }
}

#[derive(Clone, Debug, Deserialize, FromForm, Hash, Serialize)]
pub struct Credentials {
    pub email: String, 
    pub password: String,
}